class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all
  end

  def show
    @tweets = Tweet.find(params[:id])
  end

  def new
    @tweet = Tweet.new
  end

  def edit
     @tweet = Tweet.find(params[:id])
  end

  def create
    message = params[:tweet][:message]
    tdate = Time.current
    @tweet = Tweet.new(message: message, tdate: tdate)
    if @tweet.save
      redirect_to '/', notice: 'ツイートしました.'
    else
     render 'new'
    end
  end

  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to '/', notice: 'ツイートを削除しました.'
  end

  def update
    @tweet = Tweet.find(params[:id])
    message = params[:tweet][:message]
    tdate = params[:tweet][:tdate]
    @tweet.update(message: message, tdate: tdate) 
    redirect_to '/', notice: 'ツイートを更新しました.'
  end
end
